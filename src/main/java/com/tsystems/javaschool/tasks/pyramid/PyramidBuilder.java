package com.tsystems.javaschool.tasks.pyramid;
import java.util.List;

public class PyramidBuilder{

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        // TODO : Implement your solution here

        for(Integer n:inputNumbers){//проверка на входной null
            if(n==null){
                throw new CannotBuildPyramidException();
            }
        }

        Integer foundation = discriminant(inputNumbers.size());//узнаём размер основания будущей пирамиды
        System.out.println("количество строк " + foundation);
        int size = foundation*2-1;

        for (int x = 1; x < inputNumbers.size(); x++) {//сортирую по возрастанию числа
            Integer z;
            for (int y = 1; y < inputNumbers.size(); y++) {
                if (inputNumbers.get(y - 1) > inputNumbers.get(y)) {
                    z = inputNumbers.get(y - 1);
                    inputNumbers.set(y - 1, inputNumbers.get(y));
                    inputNumbers.set(y, z);
                }
            }
        }

        int[][] piramidhead = new int[foundation][size];//заполняем нулями
        for (int[] x : piramidhead) {
            for (Integer y : x) {
                y=0;
            }
        }

        int number=0;//порядковый номер забираемой цифры из inputNumbers
        int x=1;//количество цифр для добавления в строку
        for(int[]part:piramidhead){
            int start = (size-(x*2-1))/2;//определяем начало добавления чисел в строку
            int delta = 0;//шаг перескока после начала добавления
            for(int z=0;z<x;z++){
                int startnow =start+delta;
                part[startnow]=inputNumbers.get(number);
                delta=delta+2;
                number++;

            }
            if(x<=foundation){//чек на заполнение массива за счет сверки размера основания
                x++;
            }else break;
        }

        return piramidhead;
    }

    static private Integer discriminant(int z) throws CannotBuildPyramidException {//метод поиска размера основания пирамиды
        double f = ((Math.sqrt(1 + 8 * z)) - 1) / 2;
        if ((double) ((int) f) == f) {
            return (int) f;
        } else {
            throw new CannotBuildPyramidException(); }
    }
}
