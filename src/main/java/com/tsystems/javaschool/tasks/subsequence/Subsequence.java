package com.tsystems.javaschool.tasks.subsequence;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException{
        // TODO: Implement the logic here ,kg ,kg
        if(x==null || y==null){
            throw new IllegalArgumentException();
        }
        if(x.size()!=0 && y.size()==0){
            return false;
        }

        int memory = 0;//запоминает уже пройденный путь по второму списку
        for (int a = 0; a < x.size(); a++) {
            for (int b = memory; b < y.size(); b++) {
                memory++;
                if (x.get(a).equals(y.get(b))) {
                    break;
                } else if (b == y.size() - 1 && !x.get(a).equals(y.get(b))) {//если вплоть до конца эл-т не совпал- не содержит
                    return false;
                }
            }
        }
        return true;
    }
}
