package com.tsystems.javaschool.tasks.calculator;
import java.text.DecimalFormat;
import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(statement==null||statement==""){
            return null;
        }
        StringBuilder stroka = new StringBuilder(statement); //деление на куски по скобкам
        int index1;
        int index2;
        int skobka = 0;
        for (int perv = 0; perv < stroka.length(); perv++) {
            if (signCheck(stroka.charAt(perv))) {//проверка является ли знаком
                if (perv == 0 && stroka.charAt(perv) != '-') {//поверка на то, что последний символ-число, а не знак
                    return null;
                } else if (perv == stroka.length() - 1 || stroka.charAt(perv + 1) == ')') {//поверка на то, что последний символ не знак                     
                    return null;
                } else if (signCheck(stroka.charAt(perv + 1))) {//проверка на двойные знаки
                    return null;
                }
            }
            else if (numCheck(stroka.charAt(perv))) { }//цифра ли, чтобы удобнее отсекать мусор
            else if (pointCheck(stroka.charAt(perv))) {//контроль дробной точки
                if (perv == 0 || perv == stroka.length() - 1) {//точка не может быть первой или последней
                    return null;
                }
                if (!numCheck(stroka.charAt(perv + 1)) || !numCheck(stroka.charAt(perv - 1))) {//точка не может быть с краю числа
                    return null;
                }
            } else if (stroka.charAt(perv) == '(') {
                skobka++;
                if (perv != 0) {
                    if (perv == stroka.length() - 1 || numCheck(stroka.charAt(perv - 1)) || stroka.charAt(perv - 1) == ')' || stroka.charAt(perv + 1) == ')') {//контрит пустые скобки и запретную близость
                        return null;
                    }
                }
            } else if (stroka.charAt(perv) == ')') {
                skobka--;
                if (perv != stroka.length() - 1) {
                    if (perv == 0 || numCheck(stroka.charAt(perv + 1)) || stroka.charAt(perv + 1) == '(') {//контрит запретную близость
                        return null;
                    }
                }
            } else {
                return null;
            }
            if (skobka < 0) {
                return null;
            } else if (perv == stroka.length() - 1 && skobka != 0) {
                return null;
            }
        }

        while (stroka.substring(0, stroka.length()).contains(")")) {
            for (index1 = 0; index1 < stroka.length(); index1++) {
                if (stroka.charAt(index1) == ')') {//находим первую закрывающую скобку

                    for (index2 = index1; index2 >= 0; index2--) {
                        if (stroka.charAt(index2) == '(') {//находим вторую скобку
                            Double temp = partSolve(stroka.substring(index2 + 1, index1));
                            if (temp == null) {
                                return null;
                            } else {
                                String vozvrat = temp.toString();
                                stroka.replace(index2, index1 + 1, vozvrat);//замена скобочного выражение итогом решалки
                                break;
                            }
                        }
                    }
                }
            }
        }
        Double fnal = partSolve(stroka.toString());
        if (fnal == null) {
            return null;
        }

        DecimalFormat decimalFormat = new DecimalFormat("#.####");//приведение к требуемому формату

        return decimalFormat.format(fnal).replace(',','.');
    }

    private Double partSolve(String x){

        LinkedList<Double> nums = new LinkedList<>();
        LinkedList<Character> signs = new LinkedList<>();
        String line = "";
        Double o;

        for (int z = 0; z < x.length(); z++) {
            if (numCheck(x.charAt(z)) || pointCheck(x.charAt(z))) {//Проверка на то, что знак является цифрой или точкой

                line = line + x.charAt(z);//добавляем его в строку с образующимся числом
                if (z == x.length() - 1) {//проверка и добавление в список одинокого или последнего числа в строке
                    try {
                        o = Double.parseDouble(line);
                        nums.add(o);
                    } catch (NumberFormatException e) {
                        return null;
                    }
                }
            } else if (signCheck(x.charAt(z))) {//проверка на является ли знаком

                if (z == 0 && x.charAt(z) == '-') {//доп проверка на первое отрицательное число
                    line = line + x.charAt(z);//добавляем '-' к первому числу
                } else if (z == 0 && x.charAt(z) != '-') {
                    return null;
                } else if (z != 0 && x.charAt(z) == '-' && signCheck(x.charAt(z - 1))) {//проверка на второй минус от отрицательного числа после хода решений
                    line = "-";//приписываем для накопления следующего числа с минусом
                } else try {//если это простой знак
                    o = Double.parseDouble(line);//переводим накопившееся число line в double
                    nums.add(o);//переносим полученное число в список nums
                    line = "";//обнуляем для накопления нового числа
                    signs.add(x.charAt(z));//записываем текущий знак в список signs
                } catch (NumberFormatException e) {
                    return null;
                }

            } else {//все иные/недопустимые символы в пролёте
                return null;
            }
        }

        Double result;

        for (int z = 0; z < signs.size(); z++) {//количество знаков соответствует количеству действий
            if (signs.get(0) == '+' || signs.get(0) == '-') {//если + или -
                if (signs.size() > 1) {
                    if (signs.get(1) == '*') {//но второй либо *
                        result = multiplicate(nums.get(1), nums.get(2));
                        nums.set(1, result);
                        nums.remove(2);
                        signs.remove(1);
                        signs.add(' ');
                    } else if (signs.get(1) == '/') {//либо /
                        result = split(nums.get(1), nums.get(2));
                        if(result==null){
                            return null;
                        }
                        nums.set(1, result);
                        nums.remove(2);
                        signs.remove(1);
                        signs.add(' ');
                    } else if (signs.get(0) == '+') {
                        result = plus(nums.get(0), nums.get(1));
                        nums.set(0, result);
                        nums.remove(1);
                        signs.remove(0);
                        signs.add(' ');
                    } else {
                        result = minus(nums.get(0), nums.get(1));
                        nums.set(0, result);
                        nums.remove(1);
                        signs.remove(0);
                        signs.add(' ');
                    }
                } else if (signs.get(0) == '+') {
                    result = plus(nums.get(0), nums.get(1));
                    nums.set(0, result);
                    nums.remove(1);
                    signs.remove(0);
                    signs.add(' ');
                } else {
                    result = minus(nums.get(0), nums.get(1));
                    nums.set(0, result);
                    nums.remove(1);
                    signs.remove(0);
                    signs.add(' ');
                }
            } else if (signs.get(0) == '*') {
                result = multiplicate(nums.get(0), nums.get(1));
                nums.set(0, result);
                nums.remove(1);
                signs.remove(0);
                signs.add(' ');
            } else if (signs.get(0) == '/') {
                result = split(nums.get(0), nums.get(1));
                if(result==null){
                    return null;
                }
                nums.set(0, result);
                nums.remove(1);
                signs.remove(0);
                signs.add(' ');
            }
        }
        return nums.get(0);//вывод из решалки
    }

    private Double plus(Double a, Double b) {//метод сложения
        return a+b;
    }

    private Double minus(Double a, Double b) {//метод вычитания
        return a-b;
    }

    private Double multiplicate(Double a, Double b) {//метод умножения
        return a*b;
    }

    private Double split(Double a, Double b) {//метод деления
        return b!=0?a/b:null;
    }

    static private boolean numCheck(char check) {//метод проверки на число
        return Character.isDigit(check);
    }

    static private boolean signCheck(char check) {//метод проверки на знак
        return (check == '+' || check == '-' || check == '*' || check == '/');
    }

    static private boolean pointCheck(char check) {//метод проверки на точку
            return (check == '.');
    }
}
